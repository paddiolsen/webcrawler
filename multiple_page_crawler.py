#!/usr/bin/env Python3
import requests
from bs4 import BeautifulSoup

SITE_DATA = [
    {"name":"O Reilly Media", "url":"http://oreilly.com","title_tag":"h1", "body_tag":"section#product-description", "uniq_url":"http://shop.oreilly.com/product/0636920028154.do"},
    {"name":"Reuters", "url":"http://reuters.com", "title_tag":"h1","body_tag":"div.StandardArticleBody_body_1gnLA", "uniq_url": "http://www.reuters.com/article/us-usa-epa-pruitt-idUSKBN19W2D0"},
    {"name":"Brookings", "url":"http://www.brookings.edu","title_tag":"h1", "body_tag":"div.post-body", "uniq_url": "https://www.brookings.edu/blog/techtank/2016/03/01/idea-to-retire-old-methods-of-policy-education/"},
    {"name":"New York Times", "url":"http://nytimes.com","title_tag":"h1", "body_tag":"div.StoryBodyCompanionColumn div p", "uniq_url": "https://www.nytimes.com/2018/01/28/business/energy-environment/oil-boom.html"}
    ]


def get_page(url):
    try:
        r = requests.get(url)
    except requests.exceptions.RequestException:
        return None
    return BeautifulSoup(r.text, 'html.parser')
    

def safe_get(pageObj, selector):
    selected_elements = pageObj.select(selector)
    if selected_elements is not None and len(selected_elements) > 0:
        return '\n'.join([elem.get_text() for elem in selected_elements])
    return ''


def parse(site, url):
    content = {
            "url":"",
            "title": "",
            "body": ""
            }
    print(url)
    bs_obj = get_page(url)
    if bs_obj is not None:
        title = safe_get(bs_obj, site["title_tag"])
        body = safe_get(bs_obj, site["body_tag"])
        if title != '' and body != '':
            content["url"] = url
            content["title"] = title
            content["body"] = body
        print("The scraped website url is {} and the title is {}".format(content["url"], content["title"]))
        print(content["body"])



for row in SITE_DATA:
    specifik_url = row["uniq_url"] 
    parse(row, specifik_url) 
