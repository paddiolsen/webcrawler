#!/usr/bin/python3
import requests
from bs4 import BeautifulSoup


#class Content:
#    def __init__(self, url, title, body):
#        self.url = url
#        self.title = title
#        self.body = body

def getPage(url):
    r = requests.get(url)
    return BeautifulSoup(r.text, 'html.parser')

def scrapeBookings(url):
    content = {
        "url":"",
        "title":"",
        "body":""
    }
    bs = getPage(url)
    content["url"] = url
    content["title"] = bs.find('h1').text
    content["body"] = bs.find('div', {'class', 'post-body'}).text
    return content
    
url = 'https://www.brookings.edu/blog/future-development/2018/01/26/delivering-inclusive-urban-access-3-uncomfortable-truths/'
'delivering-inclusive-urban-access-3-uncomfortable-truths/'
content = scrapeBookings(url)
print ('Title: {}'.format(content["title"]))
print('URL: {}\n'.format(content["url"]))
print(content["body"])

