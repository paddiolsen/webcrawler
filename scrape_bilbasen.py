#!/usr/bin/env python3

import requests
import sqlite3
import logging
from bs4 import BeautifulSoup


logging.basicConfig(filename="bilbasen_err.log", 
            format="%(asctime)s - %(levelname)s - %(message)s", 
            level=logging.WARNING
            )


class Crawler:
    def __init__(self):
        self.headers = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"}
        self.baseurl = "https://www.bilbasen.dk/"
        #self.cardata = ()
        self.con = sqlite3.connect('bilbasen_database.db')
        self.cur = self.con.cursor()

    def db_init(self):
        self.cur.execute("""CREATE TABLE IF NOT EXISTS cars(
            car TEXT,
            price TEXT
        )""")

    def extract(self, brand, car, year_from, year_to):
        # TODO scrape all pages NOTE: add &page={page_number} to url_extended.
        url_extended = "brugt/bil/{}/{}?IncludeEngrosCVR=true&YearFrom={}&YearTo={}&PriceFrom=0&includeLeasing=false&IncludeCallForPrice=false".format(brand, car, year_from, year_to)
        try:
            r = requests.get(self.baseurl + url_extended, headers=self.headers)
            r.raise_for_status()
        except requests.exceptions.RequestException as error:
            print("connection error")
            logging.warning(error)
            return
            
        soup_obj = BeautifulSoup(r.content, "html.parser")
        
        cars = soup_obj.find_all("div", class_= "row listing listing-plus bb-listing-clickable" )
        

        for car in cars:
            name = car.find("a", class_= "listing-heading darkLink").get_text()
            price = car.find("div", "col-xs-3 listing-price").get_text()
            car_data = (name, price)
            print(("{} for a price of {}").format(name, price))
            self.insert_db(car_data)
            

    def insert_db(self, car_data):
        self.cur.execute("""INSERT INTO cars VALUES(?,?)""", car_data)
        self.con.commit()

    #def transform(self):
        # TODO monotize the data
    #    pass

    def crawle(self,):
        self.db_init()
        self.extract("vw", "Golf%20VII", "2020", "2021")
        self.con.close()

results = Crawler()
results.crawle()
